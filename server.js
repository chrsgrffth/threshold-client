'use strict';

var bodyParser = require('body-parser');
var express    = require('express');
var http       = require('http');
var fs         = require('fs')
var logger     = require('morgan');
var Path       = require('path');
var ejs        = require('ejs');

// Our server start function
module.exports = function startServer(port, path, callback) {
  var app = express();
  var server = http.createServer(app);

  // Basic middlewares: static files, logs, form fields
  app.use(express.static(Path.join(__dirname, path)));

  app.get('*', function(req, response) {

    var options = {
      host: process.env.API_HOST || 'api.threshold.dev',
      path: '/v1/settings',
      method: 'GET'
    };

    var renderedHtml;

    http.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (settings) {
        fs.readFile(__dirname + '/public/index.ejs', 'utf-8', function(err, content) {
          if (err) {
            res.end('error occurred');
            return;
          }

          settings = JSON.parse(settings)
          settings['env'] = {
            apiUrl: process.env.API_URL || 'http://api.threshold.dev/v1'
          }

          renderedHtml = ejs.render(content, settings); 
          response.send(renderedHtml);
        });
      });

      res.on('error', function(e) {
        console.log(`problem with request: ${e.message}`);
      });
    })
    .end();
  });

  // Listen on the right port, and notify Brunch once ready through `callback`.
  server.listen(port, callback);
};

if (!module.parent) {
  var PORT = process.env.PORT || 3333
  module.exports(PORT, '/public', function () {
      console.log('Server Running on ' + PORT);
    }
  );
}
