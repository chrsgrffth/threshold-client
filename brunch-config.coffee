module.exports =
  files:
    javascripts:
      joinTo: 'assets/js/app.js'
    stylesheets:
      joinTo: 'assets/css/app.css'
      sourceFiles: ['init.scss']
    templates:
      joinTo: 'app.js'
  
  server:
    path: 'server.js'
    port: 3333

  plugins:
    coffeescript:
      bare: true

    sass:
      mode: ''

